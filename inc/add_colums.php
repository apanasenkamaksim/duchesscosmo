<?php

add_filter('manage_edit-shop_order_columns', 'add_columns');
function add_columns($columns) {
	$new_columns = (is_array($columns)) ? $columns : array();
	unset($new_columns['order_actions']);
	//$new_columns['Order Total'] = '_order_total';
	$new_columns['_order_id'] = 'Order ID';
	$new_columns['_order_shipping'] = 'Shipping Total';
	$new_columns['_refund Total'] = 'Refund Total';
	$new_columns['Shipping Method'] = 'Shipping Method';
	$new_columns['_billing_first_name'] = 'Billing First Name';
	$new_columns['_billing_last_name'] = 'Shipping Last Name';
	$new_columns['Billing Name'] = 'Billing Name';
	$new_columns['Shipping Name'] = 'Shipping Name';
	$new_columns['_billing_email'] = 'Billing Email';
	$new_columns['_billing_phone'] = 'Billing Phone';
	$new_columns['Shipping First Name'] = 'Shipping First Name';
	$new_columns['_billing_address_1'] = 'Shipping Address';
	$new_columns['Shipping Address 2'] = 'Shipping Address 2';
	$new_columns['_billing_postcode'] = 'Shipping Postal Code';
	$new_columns['_billing_city'] = 'Shipping City';
	$new_columns['_billing_state'] = 'Shipping State';
	$new_columns['Shipping Country'] = 'Shipping Country';
	$new_columns['Customer Note'] = 'Customer Note';
	$new_columns['Item Name'] = 'Item Name';
	$new_columns['Item Quantity'] = 'Item Quantity';
	$new_columns['Shipping Box Size'] = 'Shipping Box Size';
	$new_columns['order_actions'] = $columns['order_actions'];
	return $new_columns;
}
add_action('manage_shop_order_posts_custom_column', 'MY_COLUMNS_VALUES_FUNCTION', 2);
function MY_COLUMNS_VALUES_FUNCTION($column) {
	global $post, $order, $woocommerce;
	$order = wc_get_order($post->ID);
	$meta_values = get_post_meta($post->ID, '', true);
	$meta = new stdClass;
	foreach ((array) get_post_meta($order->get_id()) as $k => $v) {
		$meta->$k = $v[0];
	}
	switch ($column) {
	case '_order_id':
		echo $post->ID;
		break;
	case '_billing_first_name':
		echo $meta->_billing_first_name;
		break;
	case '_billing_last_name':
		echo $meta->_billing_last_name;
		break;
	case '_billing_email':
		echo $meta->_billing_email;
		break;
	case '_billing_address_1':
		echo $meta->_billing_address_1;
		break;
	case '_billing_phone':
		echo $meta->_billing_phone;
		break;
    case 'Item Name':
        var_dump($order->get_meta('_shipping_data'));
        break;
	}

}

	}
}
add_filter("manage_edit-shop_order_sortable_columns", 'sort_colum_order'); // сортирует столбцы
function sort_colum_order($columns) {
	$custom = array(
		'_order_id' => 'Order Total',
		'Shipping Total' => 'Shipping Total',
		//  'Refund Total' => 'Refund Total',
		'_billing_first_name' => 'Billing First Name',
		'_billing_last_name' => 'Billing Last Name',
		'order_id' => 'Order ID',
		'Shipping Total' => 'Shipping Total',
		'Refund Total' => 'Refund Total',
		'Shipping Method' => 'Shipping Method',
		'_billing_first_name' => 'Billing First Name',
		'_billing_last_name' => 'Shipping Last Name',
		'Billing Name' => 'Billing Name',
		'Shipping Name' => 'Shipping Name',
		'Shipping City' => 'Shipping City',
		'_billing_email' => 'Billing Email',
		'_billing_phone' => 'Billing Phone',
		'Shipping First Name' => 'Shipping First Name',
		'_billing_address_1' => 'Shipping Address',
		'Shipping Address 2' => 'Shipping Address 2',
		'Shipping Postal Code' => 'Shipping Postal Code',
		'Shipping State' => 'Shipping State',
		'Shipping Country' => 'Shipping Country',
		'Customer Note' => 'Customer Note',
		'Item Name' => 'Item Name',
		'Item Quantity' => 'Item Quantity',
		'Shipping Box Size' => 'Shipping Box Size',
		'order_actions' => 'order_actions',
	);
	return wp_parse_args($custom, $columns);
}
