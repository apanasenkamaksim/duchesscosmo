<?php

/**
 * API request to get shipping rates from SHipping Station
 * @param $data
 * @return array|false|mixed
 */
function api_get_rates($data)
{
    $url = 'https://ssapi.shipstation.com/shipments/getrates';
    $args = [
        'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode(SHIPSTATION_API_KEY . ':' . SHIPSTATION_API_SECRET),
            'Host' => 'ssapi.shipstation.com'
        ],
        'method' => 'POST',
        'body' => $data,
        'timeout' => 10
    ];
    $cacheKey = hash('md5', $url . maybe_serialize($args));
    $cache = wp_cache_get($cacheKey);
    if (false === $cache) {
        $response = wp_remote_request($url, $args);

        $cache = [
            'state' => !is_wp_error($response) && !empty($response['response']['code']) && $response['response']['code'] == 200,
            'content' => !is_wp_error($response) ? json_decode($response['body']) : 'Error'
        ];
        wp_cache_set($cacheKey, $cache);
    }
    return $cache;
}

/**
 * To convert kg to oz
 * @param $kg
 * @return float
 */
function kgToOz($kg)
{
    return $kg * 35.274;
}

/**
 * To conver sm to inches
 * @param $sm
 * @return float
 */
function smToInches($sm)
{
    return $sm * 0.393701;
}


/**
 * To get shipping data
 * @return array|false|mixed
 */
function get_shipping_data()
{
    $cacheKey = WC()->cart->get_cart_hash();
    $cache = wp_cache_get($cacheKey);
    if (false === $cache) {
        $cache = get_shipping_data_helper();
        wp_cache_set($cacheKey, $cache);
    }
    return $cache;
}

/**
 * To prepare shipping data
 * @return array
 */
function get_shipping_data_helper() {
    try {
        if (WC()->cart && WC()->customer->get_shipping_postcode()) {
            $localDeliveryShippingFee = get_local_delivery_shipping_fee();

            if ($localDeliveryShippingFee)
                return [
                    'method' => 'Routific',
                    'cost' => $localDeliveryShippingFee * 1.12
                ];

            $notCocktails = true;
            $cocktailsTerm = get_term_by('slug', 'duchess-cosmo', 'product_cat');
            $cocktailsTerm = $cocktailsTerm->term_id;
            $cocktails = [];
            foreach (WC()->cart->get_cart() as $key => $item) {
                if (isset($item['data']) && method_exists($item['data'], 'get_id')) {
                    $product_cats = wp_get_post_terms($item['data']->get_id(), 'product_cat', array('fields' => 'ids'));
                    if (in_array($cocktailsTerm, $product_cats)) {
                        $notCocktails = false;
                        $cocktails[] = $item;
                    }
                }
            }

            if ($notCocktails) {
                return [
                    'method' => 'Routific',
                    'cost' => 8 * 1.12
                ];
            }

            $items = regroup_items($cocktails, $cocktailsTerm);
            $count = 0;
            foreach ($items as $item) {
                $count += $item['q'];
            }

            if ($count == 1) {
                $item = reset($items);
                $product = wc_get_product($item['id']);
                $cost = get_dcc_shipping_fee($product, 'canpar');
                if (!$cost)
                    throw new Exception('Not available');
                return [
                    'method' => 'Canpar',
                    'cost' => $cost,
                    'new_items' => $items
                ];
            } else if ($count == 2) {
                $canparCost = get_dcc_shipping_fee_sum($items, 'canpar');
                $canadaPostCost = get_dcc_shipping_fee_sum($items, 'canada_post');

                if (!$canparCost && !$canadaPostCost) {
                    throw new Exception('Not available');
                } else if (!$canparCost || !$canadaPostCost) {
                    return [
                        'method' => !$canparCost ? 'Canada Post' : 'Canpar',
                        'cost' => !$canparCost ? $canadaPostCost : $canparCost,
                        'new_items' => $items
                    ];
                } else {
                    $cost = min($canparCost, $canadaPostCost);
                    return [
                        'method' => $cost == $canadaPostCost ? 'Canada Post' : 'Canpar',
                        'cost' => $cost,
                        'new_items' => $items
                    ];
                }
            } else {
                throw new Exception('Not available');
            }
        }
        throw new Exception('Not available');
    } catch (Exception $e) {
        return [
            'method' => 'To be determined',
            'cost' => 0
        ];
    }
}

/**
 * To regroup cocktails (for example, 2 sets of 4 equal to 1 set of 8 and etc.)
 * @param $items
 * @param $term_id
 * @return array
 */
function regroup_items($items, $term_id)
{
    $count = 0;
    foreach ($items as $item) {
        $amount = get_post_meta($item['data']->get_id(), '_amount', true);
        $count += $amount * $item['quantity'];
    }

    $all = new WP_Query;
    $all = $all->query(array(
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $term_id
            )
        ),
        'posts_per_page' => -1
    ));

    $amount = [];
    foreach ($all as $product) {
        $a = get_post_meta($product->ID, '_amount', true);
        if (!empty($a))
            $amount[$a] = $product->ID;
    }

    krsort($amount, 1);

    $items = [];
    foreach ($amount as $a => $id) {
        if (empty($a))
            continue;

        $q = floor($count / $a);
        $count -= $q * $a;
        if (!empty($q))
            $items[$a] = [
                'q' => $q,
                'id' => $id
            ];
    }

    return $items;
}

/**
 * To get a shipping fee for a local delivery
 * V1M 3L7 - non-local delivery
 * V6N 3L7 - local delivery
 * @return false|mixed
 */
function get_local_delivery_shipping_fee()
{
    $postcode = WC()->customer->get_shipping_postcode();
    $localDeliveryPostcodes = local_delivery_postalcodes();
    foreach ($localDeliveryPostcodes as $code => $rate) {
        if (strpos($postcode, str_replace('*', '', $code)) === 0) {
            return $rate;
        }
    }

    return false;
}

/**
 * To get local delivery postal codes and the shipping fees
 * @return array
 */
function local_delivery_postalcodes()
{
    $ratePostcodes = [
        4 => [
            "V6N*",
            "V6R*",
            "V6K*",
            "V6L*",
            "V6M*",
            "V6P*",
            "V5Z*",
            "V6H*",
            "V6Z*",
            "V6E*",
            "V6G*",
            "V6C*",
            "V6B*",
            "V5Y*",
            "V5X*",
            "V5W*",
            "V5V*",
            "V5T*",
            "V6A*",
            "V5L*",
            "V5N*",
            "V5P*",
            "V5S*",
            "V5R*",
            "V5M*",
            "V5K*",
        ],
        6 => [
            "V7W*",
            "V7S*",
            "V7V*",
            "V7T*",
            "V7P*",
            "V7R*",
            "V7M*",
            "V7N*",
            "V7L*",
            "V7K*",
            "V7J*",
            "V7H*",
            "V7G*",
        ]
    ];
    $postcodes = [];
    foreach ($ratePostcodes as $rate => $codes) {
        foreach ($codes as $code) {
            $postcodes[$code] = $rate;
        }
    }
    return $postcodes;
}

/**
 * To get a final order shipping cost for non-local delivery companies
 * @param $items
 * @param $carrierCode
 * @return false|float|int
 */
function get_dcc_shipping_fee_sum($items, $carrierCode)
{
    $cost = 0;
    foreach ($items as $item) {
        $product = wc_get_product($item['id']);
        $fee = get_dcc_shipping_fee($product, $carrierCode);
        if (!$fee)
            return false;
        $cost += $fee * $item['q'];
    }

    return $cost;
}

/**
 * To get a product shipping cost for non-local delivery companies
 * @param $product
 * @param $carrierCode
 * @return false
 */
function get_dcc_shipping_fee($product, $carrierCode)
{
    $rates = api_rates_for_product($product, $carrierCode);
//    var_dump($rates);
    $serviceCode = $carrierCode == 'canada_post' ? 'expedited_parcel' : 'ground';
    $serviceCodes = array_column($rates, 'serviceCode');
    $key = array_search($serviceCode, $serviceCodes);
    if ($key !== false) {
        return $rates[$key]->shipmentCost + $rates[$key]->otherCost;
    } else {
        return false;
    }
}

/**
 * To get all product shipping rates for non-local delivery companies
 * @param $product
 * @param $carrierCode
 * @return array|mixed
 */
function api_rates_for_product($product, $carrierCode)
{
    $data = sprintf(
        '{
            "carrierCode": "%s",
            "fromPostalCode": "%s",
            "toState": "%s",
            "toCountry": "CA",
            "toPostalCode": "%s",
            "toCity": "%s",
            "weight": {
                "value": %s,
                "units": "%s"
            },
            "dimensions": {
                "units": "%s",
                "length": %s,
                "width": %s,
                "height": %s
            },
            "confirmation": "delivery",
            "residential": false
        }',
        $carrierCode,
        'V5L 1V8',
        WC()->customer->get_shipping_state(),
        WC()->customer->get_shipping_postcode(),
        WC()->customer->get_shipping_city(),
        kgToOz($product->get_weight()),
        'ounces',
        'inches',
        smToInches($product->get_length()),
        smToInches($product->get_width()),
        smToInches($product->get_height())
    );
    $result = api_get_rates($data);
    return $result['state'] ? $result['content'] : [];

}