<?php
/**
 * To add a setting amount in the package for products
 */
add_action('woocommerce_product_options_dimensions', 'amount_in_a_package_setting');
function amount_in_a_package_setting()
{
    global $post;
    $amount = get_post_meta($post->ID, '_amount', true);
    ?>
    <p class="form-field amount_field">
        <label for="product_amount"><?php esc_attr_e('Amount', 'woocommerce'); ?></label>
        <input id="product_amount" placeholder="<?php esc_attr_e('Amount', 'woocommerce'); ?>" class="short"
               type="number" name="_amount" value="<?php echo $amount; ?>"/>
    </p>
    <?php
}

/**
 * To save a setting amount in the package for products
 */
add_action('save_post', 'dcc_save_product', 10, 3);
function dcc_save_product($post_id, $post, $update)
{
    update_post_meta($post_id, '_amount', $_POST['_amount']);
}

/**
 * To change shipping cost
 */
add_filter('woocommerce_shipping_rate_cost', 'dcc_shipping_cost', 10, 2);
function dcc_shipping_cost($cost, $method)
{
    $shippingData = get_shipping_data();

    if ($shippingData)
        return $shippingData['cost'];

    return $cost;
}

/**
 * To display "To be determined" in the shipping calculator if shipping cost is not determined
 */

add_action('woocommerce_after_shipping_rate', 'dcc_woocommerce_after_shipping_rate');
function dcc_woocommerce_after_shipping_rate() {
    $shipping_data = get_shipping_data();
    if($shipping_data['method'] == 'To be determined')
        echo '<b>To be determined.</b> The manager will reach you to determine the shipping fee';
}

/**
 * To save shipping data to an order
 */
add_action('woocommerce_checkout_create_order', 'dcc_before_checkout_create_order', 20, 2);
function dcc_before_checkout_create_order( $order, $data ) {
    $order->update_meta_data( '_shipping_data', get_shipping_data() );
}